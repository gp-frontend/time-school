/*----------------------------------------
 TRANSITION SCROLL
 ----------------------------------------*/
$('.scroll').on('click', function (e) {
  // e.preventDefault();
  var anchor = $(this)
  $('html, body').stop().animate({
    scrollTop: $(anchor.attr('href')).offset().top
  }, 1000)
})
/*----------------------------------------
  PHONE MASK
----------------------------------------*/
$(".js-mask-number").mask("0#");
$(".js-mask-phone").mask("+7 (000) 000-00-00");
$(".js-mask-time").mask("00 : 00");
$(".js-mask-date").mask("00.00.0000");

/*----------------------------------------
  SELECTIZE INIT
----------------------------------------*/

$('.sumo-select').SumoSelect();

/*----------------------------------------
  TOOLTIP
----------------------------------------*/
tippy('.label-tooltip_simple');

var myTemplate = document.createElement("div");
myTemplate.innerHTML = '<ul  class="tooltip-list">' +
  '<li>' +
    '<p>Программа «Экономия»</p>' +
    '<p>3 383 руб. (10 периодов по 8 занятий)</p>' +
  '</li>' +
  '<li>' +
    '<p>Программа «Семейная скидка»:</p>' +
    '<p>10 000 руб. (3 периода по 8 занятий)</p>' +
    '<p>12 000 руб. (11 периодов по 8 занятий)</p>' +
  '</li>' +
  '<li>' +
    '<p>Программа «Гарантия цены»:</p>' +
    '<p>20 000 руб. (при обучении от 6 месяцев)</p>' +
  '</li>' +
  '</ul>';

tippy('.label-tooltip_template', {
  html: myTemplate,
});

/*----------------------------------------
  MENU
----------------------------------------*/

var buttonOpenMenu = $('.js-menu-open'),
    buttonOpenClose = $('.js-menu-close'),
    menu = $('.js-menu');

$('<div>', { class: 'menu-ovelay'}).appendTo('.header');

function openMenu (e) {
  e.preventDefault();

  menu.addClass('menu_open');
  $('.menu-ovelay').addClass('menu-ovelay_visible');
  $('body').addClass('menu-open');
}

function closeMenu (e) {
  e.preventDefault();

  menu.removeClass('menu_open');
  $('.menu-ovelay').removeClass('menu-ovelay_visible');
  $('body').removeClass('menu-open');
}

buttonOpenMenu.click(openMenu);
buttonOpenClose.click(closeMenu);
$('body .menu-ovelay').click(closeMenu);


/*----------------------------------------
  HEADER PANEL
----------------------------------------*/

var headerPanel = $('.js-header-panel'),
    buttonOpenHeaderPanel = $('.js-location'),
    buttonClosenHeaderPanel = $('.js-panel-close');

$('<div>', { class: 'panel-ovelay'}).appendTo('.header');

function openHeaderPanel (e) {
  e.preventDefault();

  buttonOpenHeaderPanel.toggleClass('header__location_active');
  headerPanel.toggleClass('header-panel_open');
  $('.panel-ovelay').toggleClass('panel-ovelay_visible');
  $('body').toggleClass('panel-open');
}

function closeHeaderPanel (e) {
  e.preventDefault();

  buttonOpenHeaderPanel.removeClass('header__location_active');
  headerPanel.removeClass('header-panel_open');
  $('.panel-ovelay').removeClass('panel-ovelay_visible');
  $('body').removeClass('panel-open');
}

buttonOpenHeaderPanel.click(openHeaderPanel);
buttonClosenHeaderPanel.click(openHeaderPanel);
$('body .panel-ovelay').click(openHeaderPanel);


/*----------------------------------------
  SLICK INIT
----------------------------------------*/

var newsCarousel = $('.js-carousel'),
    reviewsCarousel = $('.js-reviews');

newsCarousel.slick({
  infinite: false,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows:false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        dots: true
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        dots: true
      }
    },
    {
      breakpoint: 601,
      settings: {
        slidesToShow: 1,
        dots: true
      }
    }
  ]
});

reviewsCarousel.slick({
  infinite: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  dots:true,
  fade: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        fade:false
      }
    },
  ]
});

$('.js-gallery-preview').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.js-gallery-list',
  responsive: [
    {
      breakpoint: 768,
      settings: {
        fade:false,
        dots: true
      }
    },
  ]
});

$('.js-gallery-list').slick({
  touchMove: true,
  vertical: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.js-gallery-preview',
  dots: false,
  arrows: false,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        vertical: false
      }
    },
  ]
});

/*----------------------------------------
  FANCYBOX
----------------------------------------*/

$('[data-fancybox="gallery"]').fancybox({
  infobar: false,
  buttons: [
    "close"
  ],
});

/*----------------------------------------
  COLLAPSE
 ----------------------------------------*/

var buttonCollapse = $('.js-button-collapse');

function toggleCollapse() {
  $(this).toggleClass('clicked');
  $(this).closest('.js-collapse').find('.js-collapse-body').slideToggle('400');
}

buttonCollapse.on('click', toggleCollapse);
